var gulp = require('gulp');
var watch = require('gulp-watch');
var run = require('gulp-run');

var express = require('gulp-express');
var changed = require('gulp-changed');
var open = require('gulp-open');

gulp.task('full-stack', ['transmit-file'], function() {
  express.run(['./build/back_end/bin/www']);
  run('npm run dev').exec();
  var be_watcher = gulp.watch('./src/back_end/**', ['restart-server']);
});

gulp.task('restart-server', ['transmit-file'], function() {
  express.stop();
  express.run('./build/back_end/bin/www');
});

gulp.task('transmit-file', function() {
  return gulp.src('./src/back_end/**')
    .pipe(changed('./build/back_end/'))
    .pipe(gulp.dest('./build/back_end/'));
});
