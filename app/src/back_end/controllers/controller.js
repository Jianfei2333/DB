const model = require('../models/model')
const resBody = require('../utils/resBody')
const config = require('../config/general_config')
const md5 = require('md5')
const moment = require('moment')

/*
 * 测试URL
 *
 */

exports.test = (req, res, next) => {
  console.log(req.body)
  console.log('i got the message')
  resBody.success(res, 'CONNECTION_VALID')
}

/*
 * Login
 * The info of administrator was written in config file
 *
 */
exports.login = (req, res, next) => {
	console.log('in login function')
	console.log(req.body)
	console.log(config.user)
	let user = req.body
	if (req.body.username === config.user.username) {
		if (req.body.password === md5(config.user.password)) {
			//console.log('2')
			req.session.user = user
			resBody.success(res, 'LOGIN_SUCCESS')
		} else {
			//console.log('3')
			resBody.fail(res, 'WRONG_PASS')
		}
	} else {
		//console.log(4)
		resBody.fail(res, 'WRONG_USERNAME')
	}
	//resBody.success(res, 'got msg')
}

exports.checkSession = (req, res, next) => {
	console.log(req.body)
	if (req.session.user) {
		resBody.success(res, 'CHECK_SESSION_SUCCESS', req.session.user)
	} else {
		resBody.fail(res, 'CHECK_SESSION_FAIL')
	}
}

exports.checkInventory = (req, res, next) => {
	console.log(req.body)
	model.checkInventory(req.body.keyword)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_INVENTORY', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkPurchase = (req, res, next) => {
	console.log(req.body)
	console.log(req.session.user)
	model.checkPurchase(req.body.keyword)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_PURCHASE', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkPurchaseByDate = (req, res, next) => {
	console.log(req.body)
	model.checkPurchaseByDate(req.body)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_PURCHASE', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkSales = (req, res, next) => {
	console.log(req.body)
	model.checkSales(req.body.keyword)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_SALES', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkSalesByDate = (req, res, next) => {
	console.log(req.body)
	model.checkSalesByDate(req.body)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_SALES', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkReturn = (req, res, next) => {
	console.log(req.body)
	model.checkReturn(req.body.keyword)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_RETURN', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkReturnByDate = (req, res, next) => {
	console.log(req.body)
	model.checkReturnByDate(req.body)
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_SALES', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.getSuppliers = (req, res, next) => {
	console.log(req.body)
	model.checkSuppliers()
		.then((data) => {
			console.log(data)
			resBody.success(res, 'SUCCESS_GET_SUPPLIERS', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.checkSupply = (req, res, next) => {
	console.log(req.body)
	model.checkSupply(req.body)
		.then((data) => {
			console.log(data);
			resBody.success(res, 'SUCCESS_GET_SUPPLY', data)
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.addRecord = (req, res, next) => {
	console.log(req.body)
	let d = req.body
	if (d.type === 'purchase') {
		model.checkSupplyPrice(d.content)
			.then(data => {
				console.log(data)
				d.content.price = data[0].price
				d.content.date = moment(d.content.date).format('YYYY-MM-DD')
				model.addPurchase(d.content)
					.then((data) => {
						resBody.success(res, 'SUCCESS_ADD_PURCHASE')
					})
					.catch(err => {
						resBody.error(res, err)
					})
			})
			.catch(err => {
				resBody.error(res, err)
			})
	} else if (d.type === 'sales') {
		d.content.date = moment(d.content.date).format('YYYY-MM-DD')
		model.addSales(d.content) 
			.then(d => {
				resBody.success(res, 'SUCCESS_ADD_SALES')
			})
			.catch(err => {
				resBody.error(res, err)
			})
	} else if (d.type === 'return') {
		d.content.date = moment(d.content.date).format('YYYY-MM-DD')
		model.addReturn(d.content)
			.then(d => {
				resBody.success(res, 'SUCCESS_ADD_RETURN')
			})
			.catch(err => {
				resBody.error(res, err)
			})
	} else if (d.type === 'supplier') {
		model.addSupplier(d.content)
			.then(d => {
				resBody.success(res, 'SUCCESS_ADD_SUPPLIER')
			})
			.catch(err => {
				resBody.error(res, err)
			})
	} else if (d.type === 'supply') {
		model.addSupply(d.content)
			.then(d=> {
				resBody.success(res, 'SUCCESS_ADD_SUPPLY')
			})
			.catch(err => {
				resBody.error(res, err)
			})
	}
}

exports.deletePurchase = (req, res, next) => {
	console.log(req.body)
	model.deletePurchase(req.body.id)
		.then(d => {
			resBody.success(res, "SUCCESS_DELETE_PURCHASE")
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

exports.editPurchase = (req, res, next) => {
	console.log(req.body)
	req.body.date = moment(req.body.date).format('YYYY-MM-DD')
	model.editPurchase(req.body)
		.then(d => {
			resBody.success(res, "SUCCESS_EDIT_PURCHASE")
		})
		.catch(err => {
			resBody.error(res, err)
		})
}

/*
 * 退出登录
 */

exports.logout = (req, res, next) => {
  req.session.user = null;
  resBody.success(res, 'SUCCESS_LOG_OUT')
}
