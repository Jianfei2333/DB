var express = require('express');
var router_before_login = express.Router();
var router_after_login = express.Router();
var controller = require('../controllers/controller');

//login, for all user
router_before_login.post('/login', controller.login);
//check inventory infomation, for all user
router_before_login.post('/checkInventory', controller.checkInventory);

router_before_login.get('/checkSession', controller.checkSession);

//router_before_login.post('/query')
//测试URL
router_before_login.get('/testURL', controller.test)

/*
 * 其他登录前可执行操作：
 */

//router_before_login.post('/SOME_URL', controller.SOME_CONTROLLER_MIDWARE)

//退出登录
router_after_login.get('/logout', controller.logout);
//登陆后
//router_after_login.post('/modify', controller.modify);

router_after_login.post('/checkPurchase', controller.checkPurchase);

router_after_login.post('/checkPurchaseByDate', controller.checkPurchaseByDate);

router_after_login.post('/checkSales', controller.checkSales);

router_after_login.post('/checkSalesByDate', controller.checkSalesByDate);

router_after_login.post('/checkReturn', controller.checkReturn);

router_after_login.post('/checkReturnByDate', controller.checkReturnByDate);

router_after_login.get('/getSuppliers', controller.getSuppliers);

router_after_login.post('/checkSupply', controller.checkSupply);

router_after_login.post('/addRecord', controller.addRecord);

router_after_login.post('/deletePurchase', controller.deletePurchase);

router_after_login.post('/editPurchase', controller.editPurchase);

/*
 * 其他登陆后可执行操作：
 */

//router_after_login.post('/SOME_URL', controller.SOME_CONTROLLER_MIDWARE)

module.exports = {
  beforeLogin: router_before_login,
  afterLogin: router_after_login
};
