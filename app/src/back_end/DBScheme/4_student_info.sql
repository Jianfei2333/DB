CREATE TABLE student_info (
  net_id varchar(10) NOT NULL,
  student_id varchar(12) UNIQUE,
  password varchar(30) NOT NULL,
  name varchar(10),
  email varchar(30),
  PRIMARY KEY (net_id)
) default charset=utf8;
