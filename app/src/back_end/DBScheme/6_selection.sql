CREATE TABLE selection (
  net_id varchar(10) NOT NULL,
  class_id varchar(20) NOT NULL,
  PRIMARY KEY (net_id, class_id),
  FOREIGN KEY (net_id) REFERENCES student_info(net_id)
    ON DELETE CASCADE,
  FOREIGN KEY (class_id) REFERENCES class_info(class_id)
    ON DELETE CASCADE
) default charset=utf8;
