CREATE TABLE course_info (
  course_id VARCHAR(15) NOT NULL,
  course_name VARCHAR(30) DEFAULT NULL,
  grade INT DEFAULT 0,
  PRIMARY KEY (course_id)
) default charset=utf8;
