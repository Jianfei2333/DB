CREATE TABLE class_info (
  course_id VARCHAR(15) NOT NULL,
  class_id VARCHAR(20) NOT NULL,
  teacher VARCHAR(20) DEFAULT NULL,
  campus VARCHAR(2),
  PRIMARY KEY (class_id),
  FOREIGN KEY (course_id) REFERENCES course_info(course_id)
    ON DELETE CASCADE
) default charset=utf8;
