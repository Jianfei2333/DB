
module.exports = {

  db: {
    host: 'localhost',
    user: 'root',
    password: 'sysu_sac_mysql', // 数据库口令
    database: 'library' // 数据库名
  },

  session_config: {
    name: 'myapp_session',
    saveUninitialized: true,
    resave: true,
    secret: 'myapp_session_from_S6_Info_and_Computing_Science',
    unset: 'destroy',
    cookie: {
      path: '/',
      maxAge: 24 * 60 * 60 * 1000
    }
  },

  session_store: {
    host: 'localhost',
    'user': 'root',
    password: 'sysu_sac_mysql', //数据库口令
    database: 'library', // 数据库名
    schema: {
      tableName: 'sessions',
      columnNames: {
        session_id: 'sid',
        expires: 'expires',
        data: 'session'
      }
    }
  },

  user: {
    username: 'root',
    password: '****'
  }
};
