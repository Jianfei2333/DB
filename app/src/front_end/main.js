// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
//import router from './router'
//import store from './store'

import Element from 'element-ui'
import VueResource from 'vue-resource';
Vue.use(Element, { size: 'small' })
Vue.use(VueResource)

//Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  //router,
  //store,
  template: '<App/>',
  components: { App }
})
