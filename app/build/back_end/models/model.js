const  {queryDb} = require('../utils/utils')

exports.checkInventory = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM bookInventoryList \n" +
    "WHERE id LIKE ? \n" +
    "OR title LIKE ? \n" +
    "OR author LIKE ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%', '%'+d+'%'];
  return queryDb(sql, values);
}

exports.checkPurchase = (d) => {
  let sql = 
    "SELECT P.*, B.title, S.name \n" +
    "FROM purchaseList AS P \n" +
    "JOIN bookInventoryList AS B \n" +
    "ON P.bookId = B.id \n" +
    "JOIN supplierList AS S \n" +
    "ON P.supplierId = S.id"
    "WHERE id like ? \n" +
    "OR bookId like ? \n" +
    "OR supplierId like ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%', '%'+d+'%']
  return queryDb(sql, values);
}

exports.checkPurchaseByDate = (d) => {
  let sql =
    "SELECT * \n" +
    "FROM purchaseList \n" +
    "WHERE date <= ? \n" +
    "AND date >= ? \n" +
    ";";
  let values = [d.end, d.start];
  return queryDb(sql, values);
}

exports.checkSales = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM salesList \n" +
    "WHERE id like ? \n" +
    "OR bookId like ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%'];
  return queryDb(sql, values);
}

exports.checkSalesByDate = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM salesList \n" +
    "WHERE date >= ? \n" +
    "AND date <= ? \n" +
    ";";
  let values = [d.start, d.end];
  return queryDb(sql, values);
}

exports.checkReturn = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM returnList \n" +
    "WHERE id like ? \n" +
    "OR bookId like ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%']
  return queryDb(sql, values);
}

exports.checkReturnByDate = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM returnList \n" +
    "WHERE date >= ? \n" +
    "AND date <= ? \n" +
    ";";
  let values = [d.start, d.end];
  return queryDb(sql, values);
}

exports.checkSuppliers = () => {
  let sql = 
    "SElECT * \n" +
    "FROM supplierList \n" +
    ";";
  return queryDb(sql, []);
}

exports.checkSupplyByBook = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM supplyList AS S \n" +
    "JOIN bookInventoryList AS B \n" +
    "ON S.bookId = B.id \n" +
    "WHERE bookId like ? \n" +
    "OR title like ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%'];
  return queryDb(sql, values);
}

exports.checkSupplyBySupplier = (d) => {
  let sql = 
    "SELECT * \n" +
    "FROM supplyList AS S1 \n" +
    "JOIN supplierList AS S2 \n" +
    "ON S1.supplierId = S2.id \n" +
    "WHERE id like ? \n" +
    "OR name like ? \n" +
    ";";
  let values = ['%'+d+'%', '%'+d+'%'];
  return queryDb(sql, values);
}

exports.checkSupply = (dp) => {
  let sql = 
    "SELECT * \n" +
    "FROM supplyList AS S1 \n" +
    "JOIN supplierList AS S2 \n" +
    "ON S1.supplierId = S2.id \n" +
    "JOIN bookInventoryList AS B \n" +
    "ON S1.bookId = B.id \n" +
    "WHERE S2.id like ? \n" +
    "OR S2.name like ? \n" +
    "OR B.id like ? \n" +
    "OR B.title like ? \n" +
    ";";
  let values = []
  let d = dp.keyword
  if (dp.domain === '') {
    values = ['%'+d+'%', '%'+d+'%', '%'+d+'%', '%'+d+'%'];
  } else if (dp.domain === 'book') {
    values = ['', '', '%'+d+'%', '%'+d+'%']
  } else if (dp.domain === 'supplier') {
    values = ['%'+d+'%', '%'+d+'%', '', '']
  }
  console.log(values)
  return queryDb(sql, values);
}

exports.checkSupplyPrice = (dp) => {
  let sql = 
    "SELECT price \n" +
    "FROM supplyList \n" +
    "WHERE bookId = ? \n" +
    "AND supplierId = ? \n" +
    ";";
  let values = [dp.bookId, dp.supplierId]
  return queryDb(sql, values);
}

exports.addPurchase = (dp) => {
  let sql =
    "INSERT INTO purchaseList \n" +
    "(bookId, supplierId, price, amount, date) \n" +
    "VALUES (?, ?, ?, ?, ?) \n" +
    ";";
  let values = [dp.bookId, dp.supplierId, dp.price, dp.amount, dp.date];
  return queryDb(sql, values);
}

exports.addSales = (dp) => {
  let sql =
    "INSERT INTO salesList \n" +
    "(bookId, price, amount, date) \n" +
    "VALUES (?, ?, ?, ?) \n" +
    ";";
  let values = [dp.bookId, dp.price, dp.amount, dp.date]
  return queryDb(sql, values)
}

exports.addReturn = (dp) => {
  let sql =
    "INSERT INTO returnList \n" +
    "(bookId, price, amount, date) \n" +
    "VALUES (?, ?, ?, ?) \n" +
    ";";
  let values = [dp.bookId, dp.price, dp.amount, dp.date]
  return queryDb(sql, values)
}

exports.addSupplier = (dp) => {
  let sql =
    "INSERT INTO supplierList \n" +
    "(name, contactName, phoneNumber, address) \n" +
    "VALUES (?, ?, ?, ?) \n" +
    ";";
  let values = [dp.name, dp.contactName, dp.phoneNumber, dp.address]
  return queryDb(sql, values)
}

exports.addSupply = (dp) => {
  let sql =
    "INSERT INTO supplyList \n" +
    "(bookId, supplierId, price) \n"+
    "VALUES (?, ?, ?) \n" +
    ";";
  let values = [dp.bookId, dp.supplierId, dp.price]
  return queryDb(sql, values)
}

exports.deletePurchase = (id) => {
  let sql = 
    "DELETE FROM purchaseList \n" +
    "WHERE id = ? \n" +
    ";";
  let values = [id]
  return queryDb(sql, values)
}

exports.editPurchase = (dp) => {
  let sql = 
    "UPDATE purchaseList \n" +
    "SET bookId =?, supplierId =?, price =?, amount =?, date =? \n" +
    "WHERE id =? \n" +
    ";";
  let values = [dp.bookId, dp.supplierId, dp.price, dp.amount, dp.date, dp.id];
  return queryDb(sql, values) 
}