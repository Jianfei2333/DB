const  {queryDb} = require('../utils/utils')

exports.checkNetId = (net_id) => {
  let sql =
    "SELECT * \n" +
    "FROM student_info \n" +
    "WHERE net_id =? \n" +
    ";";
  let values = [net_id];
  return queryDb(sql, values)
}

exports.register = (Union_id, Net_id) => {
  let sql =
    "INSERT INTO register \n" +
    "(union_id, net_id) VALUES \n" +
    "(?, ?) \n";
    ";";
  let values = [Union_id, Net_id];
  return queryDb(sql, values)
}

exports.checkRegister = (Net_id) => {
  let sql =
    "SELECT * \n" +
    "FROM register \n" +
    "WHERE net_id =? \n" +
    ";";
  let values = [Net_id];
  return queryDb(sql, values)
}

exports.updateRegister = (union_id, net_id) => {
  let sql =
    "UPDATE register \n" +
    "SET union_id =? \n" +
    "WHERE net_id =? \n" +
    ";";
  let values = [union_id, net_id];
  return queryDb(sql, values);
}

exports.deleteRegister = (union_id) => {
  let sql =
    "DELETE FROM register\n" +
    "WHERE union_id =? \n" +
    ";";
  let values = [union_id]
  return queryDb(sql, values)
}

exports.addInfo = (info) => {
  let sql =
    "INSERT INTO student_info \n" +
    "(net_id, password) \n" +
    "VALUES (?, ?) \n" +
    ";";
  let values = [info.Net_id, info.Password];
  return queryDb(sql, values)
}


exports.modify = (attr_name, change_to, net_id) => {
  let sql =
    "UPDATE student_info \n" +
    "SET "+attr_name+" =? \n" +
    "WHERE net_id =? \n" +
    ";";
  let values = [change_to, net_id];
  return queryDb(sql, values)
}
