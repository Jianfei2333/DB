const user_model = require('../models/user-model')
const resBody = require('../utils/resBody')

/*
 * 登录:
 * 前端发送登录请求,服务端进行如下处理
 * 1.依据登录请求中的用户信息在student_info中查重判断,返回失败(密码错误)或继续
 * 2.生成128位随机session_key,并存储在session中
 * 3.添加用户信息
 * 4.添加register元组
 *
 * 在登录后的每个请求中按照如下顺序执行:
 * 1.检查session中key字段(于app.js实现)
 * 2.在register中检索对应元组,取回用户id
 * 3.在student_info中检索用户信息
 * 4.以此信息为基础进行后续操作
 *
 */

exports.register = (req, res, next) => {
  user_model.checkNetId(req.body.Net_id)
    .then((d) => {
      if (d.length !== 0) {
        if (d[0].password === req.body.Password) {
          //req.session.Union_id = generate_session()
          let Union_id = generate_session()
          user_model.checkRegister(req.body.Net_id)
            .then((d1) => {
              if (d1.length===0) {
                user_model.register(Union_id, req.body.Net_id)
                  .then(() => {resBody.success(res, 'SUCCESS_LOG_IN',{"Union_id":Union_id, "Student_info": d[0]})})
                  .catch(err => resBody.error(res, err))
              } else {
                user_model.updateRegister(Union_id, req.body.Net_id)
                  .then(() => {resBody.success(res, 'SUCCESS_UPDATE_&_LOG_IN',{"Union_id":Union_id, "Student_info": d[0]})})
                  .catch(err => resBody.error(res, err))
              }
            })
            .catch(err=>resBody.error(res, err))
        } else {
          resBody.fail(res, 'WRONG_PASSWD')
        }
      } else {
        let Union_id = ''
        user_model.addInfo(req.body)
          .then(()=>{
            //req.session.Union_id = generate_session()
            Union_id = generate_session()
          })
          .then(()=>{user_model.register(Union_id, req.body.Net_id)})
          .then(()=>{resBody.success(res, 'SUCCESS_REGISTER_&_LOG_IN',{"Union_id":Union_id, "Student_info":req.body})})
          .catch(err=>resBody.error(res, err))
      }
    })
}

function generate_session () {
  let str="0123456789!@#$%^&*()-=_+qwertyuiopasdfghjklzxcvbnm,./<>?:[]{}|"
  res = ''
  for (var i = 0; i < 128; i++) {
      lambda = (Math.random()*1000)%62
      res += (str.charAt(lambda))
  }
  return res
}

/*
 * 退出登录
 * 1.删除register该元组
 * 2.清除session中Union_id信息
 */

exports.logout = (req, res, next) => {
  user_model.deleteRegister(req.body.Union_id)
    .then(() => {
      req.body.Union_id = null;
      resBody.success(res, 'SUCCESS_LOG_OUT')
    })
    .catch(err=>resBody.error(res, err))
}

/*
 * 修改个人信息
 */

 exports.modify = (req, res, next) => {
   user_model.modify(req.body.Attr_name, req.body.Change_to, req.body.user_net_id)
    .then(() => resBody.success(res, 'SUCCESS_MODIFY'))
    .catch(err => resBody.error(res, err))
 }
