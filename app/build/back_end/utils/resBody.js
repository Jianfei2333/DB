//description字段描述具体信息内容(如:什么查询成功,什么原因失败)
//ERROR状态下直接为错误信息,并会在后台直接打印
//success状态content为返回的数据内容,如果没有则置空

exports.success = (res, description, content=null) => {
  var status = "SUCCESS"
  return res.json({
    status,
    description,
    content
  })
}

exports.fail = (res, description) => {
  var status = "FAIL"
  return res.json({
    status,
    description
  })
}

exports.error = (res, description) => {
  var status = "ERROR"
  console.log(description)
  return res.json({
    status,
    description
  })
}
