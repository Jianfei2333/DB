var express = require('express');
var router_before_login = express.Router();
var router_after_login = express.Router();
var user_controller = require('../controllers/user-controller');

/* GET users listing. */
//router_before_login.post('/checkregister', user_controller.checkregister);

router_before_login.post('/register', user_controller.register);

router_after_login.post('/logout', user_controller.logout)

router_after_login.post('/modify', user_controller.modify)

module.exports = {
  beforeLogin: router_before_login,
  afterLogin: router_after_login
};
