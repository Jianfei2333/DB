CREATE TABLE section (
  course_id varchar(15) NOT NULL,
  class_id varchar(20) NOT NULL,
  year varchar(5),
  season varchar(8),
  s_week int,
  e_week int,
  lesson varchar(8),
  room varchar(10),
  FOREIGN KEY (class_id) REFERENCES class_info(class_id)
    ON DELETE CASCADE
) default charset=utf8;
