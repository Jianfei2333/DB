CREATE TABLE register (
  union_id varchar(128) NOT NULL,
  net_id varchar(10) NOT NULL,
  PRIMARY KEY (union_id),
  FOREIGN KEY (net_id) REFERENCES student_info(net_id)
    ON DELETE CASCADE
) default charset=utf8;
