CREATE TABLE event_list (
  class_id varchar(15) NOT NULL,
  net_id varchar(10) NOT NULL,
  sub_time date,
  deadline_time date,
  type varchar(15) DEFAULT 'info',
  content varchar(200),
  question int,
  FOREIGN KEY (class_id) REFERENCES class_info(class_id)
    ON DELETE CASCADE,
  FOREIGN KEY (net_id) REFERENCES student_info(net_id)
    ON DELETE CASCADE
) default charset=utf8;
