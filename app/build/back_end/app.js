var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
//var MySQLStore = require('connect-mysql')(session);
var MySQLStore = require('express-mysql-session')(session);
var config = require('./config/general_config')
var {queryDb} = require('./utils/utils')
var pool = require('./utils/utils').pool;
var resBody = require('./utils/resBody')

var app = express();


// 路由级中间件:

var router_before_login = require('./routes/router').beforeLogin;
var router_after_login = require('./routes/router').afterLogin;


// 中间件配置:

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/src/back_end/public')));

const sc = config.session_config;
sc.store = new MySQLStore(config.session_store)
app.use(session(sc));

app.use(express.static('./src/back_end/public/'));

// 未登录状态可以访问的资源:

app.use('/router', router_before_login);

app.use((req, res, next) => {
  if (!req.session.user) {
    resBody.fail(res, 'NOT_LOGIN')
  } else {
    next()
  }
})

// 自定义路由:

app.use('/router', router_after_login);
//app.use('/', index);
//app.use('/users', users);
//app.use('/course', course);



// 其他请求抛出404

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


//异常处理
// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  resBody.error(res, err)
});

module.exports = app;
